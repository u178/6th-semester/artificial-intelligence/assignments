import math
import random
import tensorflow as tf
import cv2.cv2 as cv2
import numpy as np
import time


model = tf.keras.models.load_model('./ball_bbox_classifier')

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    raise RuntimeError("Camera is not working!")

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("Answer", cv2.WINDOW_KEEPRATIO)

width = 120
height = 160
threshold = 0.7

while cam.isOpened():
    _, image = cam.read()
    # print(image.shape)
    # break
    cropped = cv2.transpose(image)
    # print(img_trans.shape)
    # cropped = image[0:480, 0:360]
    #print(cropped.shape)
    resized = cv2.resize(cropped, (120, 160), interpolation=cv2.INTER_AREA)
    rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
    #print(rgb.shape)
    rgb = rgb / 255
    # break
    rgb = rgb.reshape(1, 160, 120, 3)
    #print(rgb.shape)
    ans = model.predict([rgb])
    if ans[0][0] > threshold:
        cv2.putText(image, f"ball: {ans[0][0]}, {ans[1][0]}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 255))
        obj_x, obj_y, w, h = ans[1][0]
        start_point = (int((obj_x - w / 2) * width), int((obj_y - h / 2) * height))
        end_point = (int((obj_x + w / 2) * width), int((obj_y + h / 2) * height))
        cv2.rectangle(rgb[0], start_point, end_point, (0, 0, 255), 1)
    else:
        cv2.putText(image, f"no ball: {ans[0][0]}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 255))
        obj_x, obj_y, w, h = ans[1][0]
        start_point = (int((obj_x - w / 2) * width), int((obj_y - h / 2) * height))
        end_point = (int((obj_x + w / 2) * width), int((obj_y + h / 2) * height))
        cv2.rectangle(rgb[0], start_point, end_point, (255, 0, 0), 1)
    to_show = cv2.transpose(rgb[0])


    #cv2.putText(image, f"{ans}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 255))
    cv2.imshow("Camera", image)
    cv2.imshow("Answer", to_show)




    key = cv2.waitKey(1)
    if key == ord('q'):
        # quit
        break

cam.release()
cv2.destroyAllWindows()
